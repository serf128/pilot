let gulp = require("gulp");
let babel = require("gulp-babel");
let concat = require('gulp-concat');
let autoprefixer = require('autoprefixer');
let uglify = require('gulp-uglify');
let postcss = require("gulp-postcss");
let cssnano = require("cssnano");
let plumber = require("gulp-plumber");
let browserSync = require('browser-sync').create();

const cssFiles = [
    'src/css/main.css'
];

const jsFiles = [
    'src/js/products.js',
    'src/js/main.js',
    'src/js/select-car-brand.js'
];
const cssLibFiles = [
    'src/css/bootstrap.min.css',
    'src/css/animate.css',
    'src/css/jquery.fancybox.min.css',
    'src/css/fontello.css',
    'src/css/owl.carousel.min.css',
    'src/css/owl.theme.default.min.css'
];

const jsLibFiles = [
    'src/js/jquery-3.4.0.min.js',
    'src/js/owl.carousel.min.js',
    'src/js/jquery.maskedinput.min.js',
    'src/js/bootstrap.min.js',
    'src/js/wow.min.js',
    'src/js/jquery.fancybox.min.js',
    'src/js/jquery.touchSwipe.min.js'
];

function css() {
    return gulp
        .src(cssFiles)
        .pipe(plumber())
        .pipe(concat('main.min.css'))
        .pipe(postcss([
            autoprefixer(),
            cssnano()
        ]))
        .pipe(gulp.dest('./dist/css/'))
        .pipe(browserSync.stream());
}

function js() {
    return gulp
        .src(jsFiles)
        .pipe(plumber())
        // .pipe(babel({
        //     presets: ['env']
        // }))
        .pipe(concat('main.min.js'))
        // .pipe(uglify({
        //     toplevel: true
        // }))
        .pipe(gulp.dest('./dist/js/'));
}
function cssLibs() {
    return gulp
        .src(cssLibFiles)
        .pipe(plumber())
        .pipe(concat('libs.min.css'))
        .pipe(postcss([
            autoprefixer(),
            cssnano()
        ]))
        .pipe(gulp.dest('./dist/css/'));
}

function jsLibs() {
    return gulp
        .src(jsLibFiles)
        .pipe(plumber())
        // .pipe(babel({
        //     presets: ['env']
        // }))
        .pipe(concat('libs.min.js'))
        .pipe(uglify({
            toplevel: true
        }))
        .pipe(gulp.dest('./dist/js/'));
}

function browserSyncInit(done) {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    done();
}

function browserSyncReload(done) {
    browserSync.reload();
    done();
}

function watchFiles() {
    gulp.watch(cssFiles, css);
    gulp.watch(jsFiles, js);
    gulp.watch('./dist/**/*', browserSyncReload);
    gulp.watch('./*.html', browserSyncReload);
}

// const build = gulp.parallel(cssLibs, jsLibs, js, css);
// const watch = gulp.parallel(cssLibs, jsLibs, js, watchFiles, browserSyncInit);
//
// exports.css = css;
// exports.cssLibs = cssLibs;
// exports.jsLibs = jsLibs;
// exports.js = js;
// exports.build = build;
// exports.watch = watch;
// exports.default = watch;

gulp.task('build', gulp.parallel(cssLibs, jsLibs, js, css));
gulp.task('watch', gulp.series(cssLibs, jsLibs, js, css, browserSyncInit, watchFiles));
gulp.task('default', gulp.series('watch'));
