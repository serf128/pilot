const products = [
    {
        "category": "classic",
        "price": 210,
        "material": "экокожа",
        "items": [
        {
            "title": "cherniy",
            "titleRu": "черный"
        },
        {
            "title": "beliy",
            "titleRu": "белый"
        },
        {
            "title": "beliy-cernaya-okantovka",
            "titleRu": "белый с черным кантом"
        },
        {
            "title": "beliy-cherniy",
            "titleRu": "белый/черный"
        },
        {
            "title": "bezheviy",
            "titleRu": "бежевый"
        },
        {
            "title": "cherniy-beliy",
            "titleRu": "черный/белый"
        },
        {
            "title": "cherniy-beliy-maika",
            "titleRu": "черный/белый майка"
        },
        {
            "title": "cherniy-bezheviy",
            "titleRu": "черный/бежевый"
        },
        {
            "title": "cherniy-bordoviy",
            "titleRu": "черный/бордовый"
        },
        {
            "title": "cherniy-korichneviy",
            "titleRu": "черный/коричневый"
        },
        {
            "title": "cherniy-krasniy",
            "titleRu": "черный/красный"
        },
        {
            "title": "cherniy-oranzheviy",
            "titleRu": "черный/оранжевый"
        },
        {
            "title": "cherniy-seriy",
            "titleRu": "черный/серый"
        },
        {
            "title": "cherniy-shokolad",
            "titleRu": "черный/шоколад"
        },
        {
            "title": "cherniy-siniy",
            "titleRu": "черный/синий"
        },
        {
            "title": "cherniy-temno-bezheviy",
            "titleRu": "черный/темно-бежевый"
        },
        {
            "title": "cherniy-temno-korichneviy",
            "titleRu": "черный/темно-коричневый"
        },
        {
            "title": "cherniy-temno-seriy",
            "titleRu": "черный/темно-серый"
        },
        {
            "title": "cherniy-velur",
            "titleRu": "черный/велюр"
        },
        {
            "title": "cherniy-zeleniy",
            "titleRu": "черный/зеленый"
        },
        {
            "title": "korichneviy",
            "titleRu": "коричневый"
        },
        {
            "title": "oranzheviy-cherniy",
            "titleRu": "оранжевый/черный"
        },
        {
            "title": "seriy",
            "titleRu": "серый"
        },
        {
            "title": "seriy-cherniy",
            "titleRu": "серый/черный"
        },
        {
            "title": "shokolad",
            "titleRu": "шоколад"
        },
        {
            "title": "temno-bezheviy",
            "titleRu": "темно-бежевый"
        },
        {
            "title": "temno-korichneviy",
            "titleRu": "темно-коричневый"
        },
        {
            "title": "temno-seriy",
            "titleRu": "темно-серый"
        }
    ]
    },
    {
        "category": "classicAlk",
        "price": 210,
        "material": "экокожа-алькантара",
        "items": [
            {
                "title": "cherniy-cherniy-alk",
                "titleRu": "черный"
            },
            {
                "title": "bezheviy-alk",
                "titleRu": "бежевый"
            },
            {
                "title": "cherniy-bezheviy-alk",
                "titleRu": "черный/бежевый"
            },
            {
                "title": "cherniy-korichneviy-alk",
                "titleRu": "черный/коричневый"
            },
            {
                "title": "cherniy-krasniy-alk",
                "titleRu": "черный/красный"
            },
            {
                "title": "cherniy-seriy-alk",
                "titleRu": "черный/серый"
            },
            {
                "title": "cherniy-shokolad-alk",
                "titleRu": "черный/шоколад"
            },
            {
                "title": "cherniy-siniy-alk",
                "titleRu": "черный/синий"
            },
            {
                "title": "cherniy-temno-bezheviy-alk",
                "titleRu": "черный/темно-бежевый"
            },
            {
                "title": "cherniy-temno-seriy-alk",
                "titleRu": "черный/темно-серый"
            },
            {
                "title": "korichneviy-alk",
                "titleRu": "коричневый"
            },
            {
                "title": "shokolad-alk",
                "titleRu": "шоколад"
            }
        ]
    },
    {
        "category": "romb",
        "price": 240,
        "material": "экокожа с ромбами",
        "items": [
            {
                "title": "cherniy-romb",
                "titleRu": "черный"
            },
            {
                "title": "beliy-cherniy-romb",
                "titleRu": "белый/черный"
            },
            {
                "title": "beliy-romb",
                "titleRu": "белый"
            },
            {
                "title": "bezheviy-romb",
                "titleRu": "бежевый"
            },
            {
                "title": "cherniy-beliy-romb",
                "titleRu": "черный/белый"
            },
            {
                "title": "cherniy-bezheviy-romb",
                "titleRu": "черный/бежевый"
            },
            {
                "title": "cherniy-bordoviy-romb",
                "titleRu": "черный/бордовый"
            },
            {
                "title": "cherniy-korichneviy-romb",
                "titleRu": "черный/коричневый"
            },
            {
                "title": "cherniy-krasniy-romb",
                "titleRu": "черный/красный"
            },
            {
                "title": "cherniy-krasniy-shov-romb",
                "titleRu": "черный с красной прострочкой"
            },
            {
                "title": "cherniy-oranzheviy-romb",
                "titleRu": "черный/оранжевый"
            },
            {
                "title": "cherniy-seriy-romb",
                "titleRu": "черный/серый"
            },
            {
                "title": "cherniy-shokolad-romb",
                "titleRu": "черный/шоколад"
            },
            {
                "title": "cherniy-siniy-romb",
                "titleRu": "черный/синий"
            },
            {
                "title": "cherniy-temno-bezheviy-romb",
                "titleRu": "черный/темно-бежевый"
            },
            {
                "title": "cherniy-temno-korichneviy-romb",
                "titleRu": "черный/темно-коричневый"
            },
            {
                "title": "cherniy-temno-seriy-romb",
                "titleRu": "черный/темно-серый"
            },
            {
                "title": "korichneviy-bezheviy-romb",
                "titleRu": "коричневый/бежевый"
            },
            {
                "title": "korichneviy-romb",
                "titleRu": "коричневый"
            },
            {
                "title": "oranzheviy-cherniy-romb",
                "titleRu": "оранжевый/черный"
            },
            {
                "title": "seriy-cherniy-romb",
                "titleRu": "серый/черный"
            },
            {
                "title": "seriy-romb",
                "titleRu": "серый"
            },
            {
                "title": "shokolad-romb",
                "titleRu": "шоколад"
            },
            {
                "title": "temno-bezheviy-romb",
                "titleRu": "темно-бежевый"
            },
            {
                "title": "temno-korichneviy-romb",
                "titleRu": "темно-коричневый"
            },
            {
                "title": "temno-seriy-romb",
                "titleRu": "темно-серый"
            }

        ]
    },
    {
       "category": "rombAlk",
        "price": 240,
        "material": "экокожа с ромбами - алькантара",
        "items": [
            {
                "title": "cherniy-alk-romb",
                "titleRu": "черный"
            },
            {
                "title": "bezheviy-alk-romb",
                "titleRu": "бежевый"
            },
            {
                "title": "cherniy-bezheviy-alk-romb",
                "titleRu": "черный/бежевый"
            },
            {
                "title": "cherniy-korichneviy-alk-romb",
                "titleRu": "черный/коричневый"
            },
            {
                "title": "cherniy-krasniy-alk-romb",
                "titleRu": "черный/красный"
            },
            {
                "title": "cherniy-shokolad-alk-romb",
                "titleRu": "черный/шоколад"
            },
            {
                "title": "cherniy-siniy-alk-romb",
                "titleRu": "черный/синий"
            },
            {
                "title": "cherniy-svetlo-seriy-alk-romb",
                "titleRu": "черный/светло-серый"
            },
            {
                "title": "korichneviy-alk-romb",
                "titleRu": "коричневый"
            },
            {
                "title": "shokolad-alk-romb",
                "titleRu": "шоколад"
            }

        ]
    }

];