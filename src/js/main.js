﻿// ================= constructor ======================
const installPrice = 50;

let currentProduct = {
    "category": 0,
    "item": 0
};

let Item = function (category, title) {
    this.title = title;
    this.titleRU = getTitleRU(category, title);
    this.category = getCategory(category);
    this.html = `<li class="item" id="${this.title}">
                    <img src="/images/${this.category}/x60/${this.title}.jpg" alt="${this.titleRU}">
                   </li>`;
};

let PreviewItem = function (category, title) {
    this.title = title;
    this.titleRU = getTitleRU(category, title);
    this.category = getCategory(category);
    this.html = `<a class="preview-a" data-fancybox="products" href="/images/${this.category}/${this.title}.jpg" data-caption="Чехол на сиденье из экокожи ${this.titleRU}">
                <img class="preview-img" src="/images/${this.category}/x300/${this.title}.jpg" alt="Чехол на сиденье из экокожи ${this.titleRU}">
                <p class="color">цвет: <span>${this.titleRU}</span></p>
                <i class="zoom icon icon-zoom-in"></i>
                </a>`;

};

$(".change_radio").on("change", function () {
    if (this.checked) {
        currentProduct.category = $(".change_radio").index(this);
        currentProduct.item = 0;
        catalogGalleryBuilder();
    }
});

$("#catalog-gallery").on("click", "li", function () {
    clearBorderItem ();
    currentProduct.item = $(this).index();
    changeCurrentProduct();
});

$('#gridCheck1').on('change', function () {
    let price = 0;
    if (this.checked) {
        price = products[currentProduct.category].price + installPrice;
    } else {
        price = products[currentProduct.category].price;
    }
    $(".price").html(`цена: ${price} руб.`);
});

$(".left-arrow").on("click", "a",function (e) {
    e.preventDefault();
    counterDown(1);
});
$(".right-arrow").on("click", "a",function (e) {
    e.preventDefault();
    counterUp(1);
});

catalogGalleryBuilder();

$(function () {
    $(".preview-container, .catalog-gallery").swipe ({
        swipe:function(event, direction, distance, duration, fingers, fingerData, currentDirection) {
            let count = +(distance*duration/10000).toFixed();
            if (direction === 'left' ) {
                counterUp(count);
            }
            if (direction === 'right' ) {
                counterDown(count);
            }
        }
    })
    .swipe( {allowPageScroll:"auto"} );
});

function catalogGalleryBuilder() {
    $("#catalog-gallery,.preview-wrapper").empty();
    for (let i = 0; i < products[currentProduct.category].items.length; i++) {
        $("#catalog-gallery")
            .append(new Item(currentProduct.category, products[currentProduct.category].items[i]["title"]).html);
        $(".preview-wrapper")
            .append(new PreviewItem(currentProduct.category, products[currentProduct.category].items[i]["title"]).html);
    }
    changeCurrentProduct();
}

function changeCurrentProduct() {
    $('.preview-wrapper').css({
        "transform": `translateX(-${currentProduct.item*300}px)`
    });
    if ($(window).width() <= '786') {
        $("#catalog-gallery").css({
            "transform": `translateX(-${currentProduct.item*64}px)`
        })
    }
    $(`#${products[currentProduct.category].items[currentProduct.item]["title"]}`)
         .css("border", "2px solid #E6551F");
    $(".color")
         .html(`цвет: <span> ${products[currentProduct.category].items[currentProduct.item]["titleRu"]} </span>`);
    $(".material").html(`материал: <span> ${products[currentProduct.category].material} </span>`);
    $(".price").html(`цена: ${products[currentProduct.category].price}  руб.`);
    $(".order-preview")
        .empty()
        .attr({
         src: `/images/${products[currentProduct.category].category}/x60/${products[currentProduct.category].items[currentProduct.item]["title"]}.jpg`,
         alt: `Чехол на сиденье из экокожи ${products[currentProduct.category].items[currentProduct.item]["titleRu"]}`
    });
}

function counterUp(count) {
    if (currentProduct.item + count < (products[currentProduct.category].items.length - 1)) {
        clearBorderItem();
        currentProduct.item += count;
        changeCurrentProduct();
    } else {
        clearBorderItem();
        currentProduct.item = (products[currentProduct.category].items.length - 1);
        changeCurrentProduct();
    }
}

function counterDown(count) {
    if (currentProduct.item-count > 0) {
        clearBorderItem();
        currentProduct.item -= count;
        changeCurrentProduct();
    } else {
        clearBorderItem();
        currentProduct.item = 0;
        changeCurrentProduct();
    }
}

function getTitle(title) {
    return products[currentProduct.category].items[title]["title"]
}

function getTitleRU(category, title) {
    for (let i = 0; i < products[category].items.length; i++) {
        if (products[category].items[i]["title"] === title) {
            return products[category].items[i]["titleRu"];
        }
    }
}

function getCategory(category) {
    return products[category].category;
}

function clearBorderItem () {
    $(`#${products[currentProduct.category].items[currentProduct.item]["title"]}`).css("border", "2px solid #fff");
}

//==================== end constructor ==============================
//==================== main form submit =============================
jQuery(document).ready(function () {
    $("#mainForm").submit(function () {
        let data = $("#mainForm").serializeArray();
        let order = `${products[currentProduct.category].material}__${products[currentProduct.category].items[currentProduct.item]["titleRu"]}`;
        data[data.length] = {
            'name': 'product',
            'value': order
        };
        $.ajax({
            url: "main-form-handler.php",
            type: "POST",
            dataType: "html",
            data: data,
            success: function () {
                $('#messegeResult').html('<p style="color:green;font-weight:bold">Сообщение успешно отправлено. Скоро Вам перезвонят</p>');
            },
            error: function () {
                $('#messegeResult').html('<p style="color:red;font-weight:bold">Возникла ошибка при отправке формы. Попробуйте еще раз</p>');
            }
        });

        $(':input', '#mainForm')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
        return false;
    });
});
//==================== end main form submit =============================
//==================== zakaz zvonka submit =============================
jQuery(document).ready(function () {
    $("#zakaz_zvonka").submit(function () {
        let data = $("#zakaz_zvonka").serializeArray();
        $.ajax({
            url: "zakaz_zvonka-handler.php",
            type: "POST",
            dataType: "html",
            data: data,
            success: function () {
                $('#messegeResult2').html('<p style="color:#56fb56;font-weight:bold">Сообщение успешно отправлено. Скоро Вам перезвонят</p>');
            },
            error: function () {
                $('#messegeResult2').html('<p style="color:red;font-weight:bold">Возникла ошибка при отправке формы. Попробуйте еще раз</p>');
            }
        });

        return false;
    });
});
//==================== end zakaz zvonka submit =============================
//=================== scroll ============================================

var $page = $('html, body');
$('a[href*="#"]:not([href="#reviews-carousel"])').click(function () {
    $page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 600);
    return false;
});

//=================== end scroll =======================================
//=================== phone mask =======================================

jQuery(document).ready(function () {
    $('#inputPhone').mask('+375 (99) 999 99 99');
    $('#recipient-phone').mask('+375 (99) 999 99 99');
});

//=================== end phone mask ===================================
//==================== main slider =====================================

jQuery(document).ready(function () {
    $('.podlokotniki-carousel').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        autoHeight: true,
        lazyLoad: true,
        center: true,
        smartSpeed: 500,
        autoplay: 6000,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            800: {
                items: 4
            },
            1200: {
                items: 4
            }
        }
    });

    showSliders();

    $(window).on("resize", function () {
        showSliders();
    });
});

function showSliders() {
    if ($(window).width() <= '786') {
        initialMatsSlider();
    } else {
        destroyMatsSlider();
    }
}

function initialMatsSlider() {
    $('.mats-carousel')
        .trigger("destroy.owl.carousel").removeClass("owl-carousel")
        .addClass("owl-carousel").owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        loop: true,
        margin: 10,
        nav: false,
        autoHeight: true,
        lazyLoad: true,
        center: true,
        smartSpeed: 600,
        autoplay: 6000,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 2
            },
            800: {
                items: 4
            },
            1200: {
                items: 4
            }
        }
    });
}

function destroyMatsSlider() {
    $('.mats-carousel').trigger("destroy.owl.carousel").removeClass("owl-carousel");
}
// ========================= capes preview =======================================
var currentCapePreview = 1;

function capePreview(n) {
    $("#cape-preview" + currentCapePreview).attr("style", "display:none");
    $("#cape-preview" + n).attr("style", "display:block");
    $("#cape-control" + currentCapePreview).css("border", "2px solid transparent");
    $("#cape-control" + n).css("border", "2px solid #E6551F");
    currentCapePreview = n;

}
// ========================= end capes preview =======================================
//========================== modal ===================================================
jQuery(document).ready(function () {

    $('.js-button-campaign').click(function () {
        $('.overflow').css('filter', 'blur(5px)');
        $('.js-overlay-campaign')
            .fadeIn()
            .addClass('disabled');
    });

    $('.js-close-campaign').click(function () {
        $('.js-overlay-campaign').fadeOut();
        $('.overflow').css('filter', 'none');
    });

    $(document).mouseup(function (e) {
        var popup = $('.js-popup-campaign');
        if (e.target != popup[0] && popup.has(e.target).length === 0) {
            $('.js-overlay-campaign').fadeOut();
            $('.overflow').css('filter', 'none');
        }
    });


//==================== end modal ======================================
//==================== youtube lazy ===================================
    if ($(window).width() >= '786') {
        $(function() {
            $(".youtube").each(function() {
                $(this).css('background-image', 'url(http://i.ytimg.com/vi/' + this.id + '/maxresdefault.jpg)');
                $(document).delegate('#'+this.id, 'click', function() {
                    let iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1";
                    if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
                    let iframe = $('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': $(this).width(), 'height': $(this).height() });
                    $(this).replaceWith(iframe);
                });
            });
        });
    } else {
        $(".youtube").each(function() {
            let iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1";
            if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
            let iframe = $('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': $(this).width(), 'height': $(this).height() });
            $(this).replaceWith(iframe);
        });
    }


});